# MFRC522 IoT-Pi Module
Simple module to get the uid of MFRC522 for IoT-Pi framework.

> **Note:** [MFRC522-python](https://github.com/pimylifeup/MFRC522-python) library is required to be installed in the virtual environment

## Installation

1. Put `mfrc522.py` in `iotpi/modules/` folder
2. Import the module in `iotpi/init.py`:
    ```python
    import iotpi.modules.mfrc522 as mfrc522
    self.reader = mfrc522.module()
    ```
3. Read UID in `main.py`:
    ```python
    print("ID: " + pi.reader.read())
    ```